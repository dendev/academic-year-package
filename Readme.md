# Academic Year

> gestion de la date de l'année académique

Permet de connaitre l'année académique d'une date sous différents format.   
Ex: J'ai besoin de savoir l'année académique dans laquelle je me trouve.

## Installation

```bash
composer config repositories.academic_year vcs https://gitlab.com/dendev/academic-year-package.git
composer require dendev/academic_year
```

## Utilisation
La facade **AcademicYearManager** est disponible au sein de laravel
```php
$academic_year = \AcademicYearManager::current();
```

## Docs

Installer [doctum](https://github.com/code-lts/doctum)
```bash
curl -O https://doctum.long-term.support/releases/latest/doctum.phar
php doctum.phar update doctum.php
google-chrome ./docs/build/index.html
```

## Test
Executer tous les tests
```bash
phpunit 
```

Executer un jeu de test spécifique
```bash
phpunit tests/Unit/LeodelManagerTest.php
```

Executer un test spécifique
```bash
phpunit --filter testIsTeacher 
```

## Metrics

Installer [phpmetrics](https://phpmetrics.org/)
```bash
composer global require 'phpmetrics/phpmetrics'
phpmetrics --report-html=metrics ./src --junit='phpunit.xml'
google-chrome metrics/index.html
```

## ?
https://gitlab.com/help/user/packages/composer_repository/index.md
