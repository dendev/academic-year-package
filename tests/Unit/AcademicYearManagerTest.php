<?php

namespace Tests\Unit;

use Illuminate\Support\Carbon;
use Orchestra\Testbench\TestCase;

class AcademicYearManagerTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [
            'Dendev\AcademicYear\AddonServiceProvider',
        ];
    }

    //
    public function testBasic()
    {
        $exist = \AcademicYearManager::test_me();
        $this->assertEquals('academic_year_manager', $exist);
    }

    public function testFromDate()
    {
        $date = Carbon::createFromFormat('d/m/Y', '10/08/2019'); // 2018-2019

        $current_year_default = \AcademicYearManager::from_date($date);
        $current_year_etnic = \AcademicYearManager::from_date($date, 'etnic');
        $current_year_sheldon = \AcademicYearManager::from_date($date, 'sheldon');
        $current_year_proecohenallux = \AcademicYearManager::from_date($date, 'proecohenallux');

        $this->assertEquals($current_year_default, '2018');
        $this->assertEquals($current_year_etnic, '1819');
        $this->assertEquals($current_year_sheldon, '2018');
        $this->assertEquals($current_year_proecohenallux, '2018');
    }

    public function testFromDateCurrent()
    {
        $date = Carbon::createFromFormat('d/m/Y', '10/10/2019'); // 2019-2020

        $current_year = \AcademicYearManager::from_date($date);
        $current_year_etnic = \AcademicYearManager::from_date($date, 'etnic');
        $current_year_sheldon = \AcademicYearManager::from_date($date, 'sheldon');
        $current_year_proecohenallux = \AcademicYearManager::from_date($date, 'proecohenallux');

        $this->assertEquals($current_year, '2019');
        $this->assertEquals($current_year_etnic, '1920');
        $this->assertEquals($current_year_sheldon, '2019');
        $this->assertEquals($current_year_proecohenallux, '2019');
    }

    public function testFromDateNextYearCurrent()
    {
        $date = Carbon::createFromFormat('d/m/Y', '10/05/2020'); // 2019-2020

        $current_year = \AcademicYearManager::from_date($date);
        $current_year_etnic = \AcademicYearManager::from_date($date, 'etnic');
        $current_year_sheldon = \AcademicYearManager::from_date($date, 'sheldon');
        $current_year_proecohenallux = \AcademicYearManager::from_date($date, 'proecohenallux');

        $this->assertEquals($current_year, '2019');
        $this->assertEquals($current_year_etnic, '1920');
        $this->assertEquals($current_year_sheldon, '2019');
        $this->assertEquals($current_year_proecohenallux, '2019');
    }

    public function testFromDateBeforeCurrent()
    {
        $date = Carbon::createFromFormat('d/m/Y', '31/08/2020'); // 2019-2020

        $current_year = \AcademicYearManager::from_date($date);
        $current_year_etnic = \AcademicYearManager::from_date($date, 'etnic');
        $current_year_sheldon = \AcademicYearManager::from_date($date, 'sheldon');
        $current_year_proecohenallux = \AcademicYearManager::from_date($date, 'proecohenallux');

        $this->assertEquals($current_year, '2019');
        $this->assertEquals($current_year_etnic, '1920');
        $this->assertEquals($current_year_sheldon, '2019');
        $this->assertEquals($current_year_proecohenallux, '2019');
    }

    public function testFromDateAfterCurrent()
    {
        $date = Carbon::createFromFormat('d/m/Y', '31/11/2020'); // 2020-2021

        $current_year = \AcademicYearManager::from_date($date);
        $current_year_etnic = \AcademicYearManager::from_date($date, 'etnic');
        $current_year_sheldon = \AcademicYearManager::from_date($date, 'sheldon');
        $current_year_proecohenallux = \AcademicYearManager::from_date($date, 'proecohenallux');

        $this->assertEquals($current_year, '2020');
        $this->assertEquals($current_year_etnic, '2021');
        $this->assertEquals($current_year_sheldon, '2020');
        $this->assertEquals($current_year_proecohenallux, '2020');
    }

    //
    public function testBegintAt()
    {
        $date = Carbon::createFromFormat('d/m/Y', '01/03/2020'); // 2019-2020
        $ok_begin_at = Carbon::createFromFormat('d/m/Y', '14/09/2019'); // 2019-2020

        $begin_at = \AcademicYearManager::begin_at($date);
        $this->assertEquals($begin_at, $ok_begin_at);
    }

    public function testEndAt()
    {
        $date = Carbon::createFromFormat('d/m/Y', '01/03/2020'); // 2019-2020
        $ok_end_at = Carbon::createFromFormat('d/m/Y', '13/09/2020'); // 2019-2020

        $end_at = \AcademicYearManager::end_at($date);
        $this->assertEquals($end_at, $ok_end_at);
    }

    //
    public function testIsDuring()
    {
        $date = Carbon::createFromFormat('d/m/Y', '01/03/2020'); // 2019-2020

        $is_during = \AcademicYearManager::is_during(2019, $date);
        $is_during_etnic = \AcademicYearManager::is_during(1920, $date, 'etnic');
        $is_during_sheldon = \AcademicYearManager::is_during(2019, $date, 'sheldon');
        $is_during_proecohenallux = \AcademicYearManager::is_during(2019, $date, 'proecohenallux');

        $this->assertTrue($is_during);
        $this->assertTrue($is_during_etnic);
        $this->assertTrue($is_during_sheldon);
        $this->assertTrue($is_during_proecohenallux);
    }

    public function testToFormat()
    {
        $date = Carbon::createFromFormat('d/m/Y', '31/11/2020'); // 2020 - 2021

        $current_year = \AcademicYearManager::from_date($date);
        $current_year_etnic = \AcademicYearManager::from_date($date, 'etnic');

        $current_year_to_etnic_format = \AcademicYearManager::to_format($current_year, false, 'etnic');
        $current_year_etnic_to_default_format = \AcademicYearManager::to_format($current_year, 'etnic', 'default');


        $this->assertEquals($current_year_etnic, $current_year_to_etnic_format );
        $this->assertEquals($current_year_etnic_to_default_format, $current_year);
        $this->assertEquals($current_year_etnic, $current_year_to_etnic_format );
    }

    public function testLabel()
    {
        $date = Carbon::createFromFormat('d/m/Y', '31/11/2020'); // 2020 - 2021

        $current_year = \AcademicYearManager::from_date($date);
        $current_year_sheldon = \AcademicYearManager::from_date($date, 'sheldon');
        $current_year_proecohenallux = \AcademicYearManager::from_date($date, 'proecohenallux');

        $current_year_label = \AcademicYearManager::label($current_year, false );
        $current_year_sheldon_label = \AcademicYearManager::label($current_year, 'sheldon');
        $current_year_proecohenallux_label = \AcademicYearManager::label($current_year, 'proecohenallux');

        $this->assertEquals($current_year_label, '2020 - 2021');
        $this->assertEquals($current_year_sheldon_label, '2020 - 2021');
        $this->assertEquals($current_year_proecohenallux_label, '2020 - 2021');
    }
}
