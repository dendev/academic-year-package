<?php

namespace Dendev\AcademicYear;

use Illuminate\Support\ServiceProvider;

class AddonServiceProvider extends ServiceProvider
{
    use AutomaticServiceProvider;

    protected $vendorName = 'dendev';
    protected $packageName = 'academic_year';
    protected $commands = [];
}
