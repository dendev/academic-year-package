<?php

namespace Dendev\AcademicYear\Providers;

use Dendev\AcademicYear\Services\AcademicYearManagerService;
use Illuminate\Support\ServiceProvider;

class AcademicYearServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('academic_year_manager', function ($app) {
            return new AcademicYearManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
