<?php

namespace Dendev\AcademicYear\Facades;

use Illuminate\Support\Facades\Facade;

class AcademicYearManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'academic_year_manager';
    }
}
