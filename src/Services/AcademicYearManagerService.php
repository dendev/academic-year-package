<?php

namespace Dendev\AcademicYear\Services;


use Illuminate\Support\Carbon;

class AcademicYearManagerService
{
    private static $_begin_day_number = 254;
    private static $_fist_day = 14;
    private static $_last_day = 13;

    /**
     * Retourne l'année académiquer courante.
     *
     * L'année académique 2019-2020 doit retourner 2019
     *
     * @return bool|int
     */
    public function current($format = false): int
    {
        return $this->from_date(Carbon::now(), $format);
    }

    /**
     * Retourne l'année académiquer précédente.
     *
     * L'année académique 2019-2020 doit retourner 2018
     *
     * @return bool|int
     */
    public function previous($format = false): int
    {
        return $this->from_date(Carbon::now()->subYear(), $format);
    }

    /**
     * Retourne l'année académiquer suivante.
     *
     * L'année académique 2019-2020 doit retourner 2020
     *
     * @return bool|int
     */
    public function next($format = false): int
    {
        return $this->from_date(Carbon::now()->addYear(), $format);
    }

    /**
     * Donne l'année académique de la date donnée en argument
     *
     * Le format de la date est donnée d'aprés l'arguement date
     *
     * format etnic pour une date 19-20 ( année 2019 - 2020 )
     * format sheldon pour une date 2019   ( année 2019 - 2020 )
     * format proeco pour une date 2019   ( année 2019 - 2020 )
     * aucune format pour l'année courante
     *
     * @param $date
     * @param $format chaine ayant pour valeur sheldon, proeco ou false
     * @return int
     * @throws \Exception
     */
    public function from_date(Carbon $date, $format = false) : int // format sheldon is wrong is format datt
    {
        $academic_year = false;
        if($date->dayOfYear() < self::$_begin_day_number) // 10 septembre
        {
            if( $format == 'etnic' ) // ex: 18-19 pour 2018 - 2019 // etnic
            {
                $academic_year = $date->format('y') - 1 . $date->format('y');
            }
            else if( $format == 'proeco' || $format == 'proecohenallux' || $format == 'sheldon' ) // ex:  2018 pour 2018 - 2019
            {
                $academic_year = $date->format('Y') - 1;
            }
            else // ex 2018 pour 2018 - 2019
            {
                $academic_year = $date->format('Y') - 1;
            }
        }
        else
        {
            if( $format == 'etnic' ) // ex: 18-19 pour 2019 - 2020
            {
                $academic_year = $date->format('y')  . ( (int)$date->format('y') + 1 );
            }
            else if( $format == 'proeco' || $format == 'proecohenallux' ) // ex:  2018 pour 2019 - 2020
            {
                $academic_year = $date->format('Y');
            }
            else if( $format == 'sheldon' ) // ex:  2018 pour 2019 - 2020
            {
                $academic_year = $date->format('Y');
            }
            else // ex 2019 pour 2019 - 2020
            {
                $academic_year = (int)$date->format('Y') ;
            }
        }


        return $academic_year;
    }

    /**
     * Renvoi la date de début de l'année académique courante
     *
     * @return \Carbon\Carbon
     */
    public function current_begin_at() : \Carbon\Carbon
    {
        return self::begin_at(Carbon::now());
    }

    /**
     * Renvoi la date de début de l'année académique de la date donnée
     *
     * @param $date Carbon date dont on veut connaitre la rentrée academique
     * @return \Carbon\Carbon
     */
    public function begin_at(Carbon $date) : \Carbon\Carbon
    {
        $year = self::from_date($date);
        $begin_date = "$year-09-" . self::$_fist_day;

        return Carbon::createFromFormat('Y-m-d', $begin_date);
    }

    /**
     * Renvoi la date de find de l'année académique courante
     *
     * @return \Carbon\Carbon
     */
    public function current_end_at() : \Carbon\Carbon
    {
        return self::end_at(Carbon::now());
    }

    /**
     * Renvoi la date de fin de l'année académique de la date donnée
     *
     * @param Carbon $date
     * @return \Carbon\Carbon
     */
    public function end_at(Carbon $date)
    {
        $year = $date->year;
        $end_date = "$year-09-" . self::$_last_day;

        return Carbon::createFromFormat('Y-m-d', $end_date);
    }

    /**
     * Vérifie si la date donnée se trouve dans l'année académique fournis
     *
     * @param $academic_year
     * @param $date
     * @param bool $format
     * @return bool
     */
    public function is_during($academic_year, $date, $format = false) : bool
    {
        $academic_year_found = self::from_date($date, $format);
        return ( $academic_year == $academic_year_found );
    }

    /**
     * Renvoi l'année académique sous forme de text affichable pour la date donnée.
     *
     * @param Carbon $date
     * @return string
     */
    public function label($academic_year, $format = false) : string
    {
        $label = false;

        if( $format == 'etnic')
        {
            $academic_year = self::to_format($academic_year, 'etnic', false);
        }


        $label = $academic_year . ' - ' . ( $academic_year + 1 );

        return $label;
    }

    /**
     * Transforme une date académique dans un autre format
     *
     * @param $academic_year
     * @param $original_format
     * @param $final_format
     * @return bool|string
     */
    public function to_format($academic_year, $original_format, $final_format)
    {
        $academic_year_formated = false;

        if( $original_format == 'etnic') // 1920
        {
            $tmp = str_split( $academic_year );
            $academic_year_formated = "20" . $tmp[0] . $tmp[1]; // time validity < 100 years
        }
        else // 2019
        {
            $academic_year_formated = $academic_year;
            /*
            $tmp = str_split( $academic_year );
            if( $tmp[2] == 9 )
            {
                $next_year_1 = $tmp[2] + 1;
                $next_year_2 = 0;
            }
            else
            {
                $next_year_1 = $tmp[2];
                $next_year_2 = $tmp[3] + 1;
            }

            $academic_year_formated = $tmp[2] . $tmp[3] . $next_year_1 . $next_year_2;
            */
        }

        if( $final_format === 'etnic')
        {
            $tmp = str_split( $academic_year );
            $a = $tmp[0];
            $b = $tmp[1];
            $d = ($tmp[3] !== 9 ) ? $tmp[3] + 1: 0;
            $c = ( $b !== 0 ) ? $tmp[2] : $tmp[2]+1;
            $academic_year_final = $a.$b.$c.$d;
        }
        else
        {
            $academic_year_final = $academic_year_formated;
        }

        return $academic_year_final;
    }

    public function test_me()
    {
        return "academic_year_manager";
    }
}
